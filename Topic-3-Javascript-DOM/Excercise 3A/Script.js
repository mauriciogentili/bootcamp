function getJokes() {
  const requestURL = 'http://api.icndb.com/jokes/random';

  showJokes({ data: {}, error:"", isLoading: true });

  fetch(requestURL, {
    method: "GET"
  })
    .then(response =>  response?.json()) 
    .then(data => {
      showJokes({ data, error: "", isLoading: false });
    })
    .catch (error => {
      showJokes({ data: {}, error: error.message, isLoading: false })
    }) 
    
}

const [button] = document.getElementsByTagName("button");
button.onclick = () => getJokes();

function showJokes(dataJokes){
  const showContent = document.getElementById('showContent');
  const paragraph = document.createElement('p');
  showContent.innerHTML = '';
  
  /* loading */
  if(dataJokes.isLoading){ 
    //showContent.innerHTML = '';
    paragraph.textContent = "Searching Jokes..."
    showContent.appendChild(paragraph);  
    return;
  } 
  
  /* error */ 
  if (dataJokes.error){
    //showContent.innerHTML = '';
    paragraph.textContent = dataJokes.error;
    paragraph.className = 'error';
    showContent.appendChild(paragraph);
       
    return;
  }
  
  console.log(dataJokes);

  
  /* carga correcta */
  const myArticle = document.createElement('article');
  const paragraph2 = document.createElement('p');
  const paragraph3 = document.createElement('p');
  const myList = document.createElement('ul');
  
  const jokes = dataJokes.data.value;
  //showContent.innerHTML = '';
  
  paragraph.textContent = 'Id: ' + jokes.id;
  paragraph2.textContent = 'Joke: ' + jokes.joke;

  var categories = jokes.categories;
  if (categories.length > 0) {
    paragraph3.textContent = 'Categories: ';
  }
  categories.forEach(category => {
    var listItem = document.createElement('li');
    listItem.textContent = category;
    myList.appendChild(listItem);
  });
  

  myArticle.appendChild(paragraph);
  myArticle.appendChild(paragraph2);
  myArticle.appendChild(paragraph3);
  myArticle.appendChild(myList);
  myArticle.className = "jokes"
  showContent.appendChild(myArticle);

}
