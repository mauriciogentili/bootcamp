function getRepositories(arg) {
  console.log('argumento:' + arg);
  const requestURL = 'https://api.github.com/search/repositories?q=' + arg;
  
  /*
  const requestURL = 'https://api.github.com/search/repositories?q=JavaScript';
  
  if (arg != '') {
    requestURL = 'https://api.github.com/search/repositories?q=' + arg;
    
  }
  */
  showRepo({ data: {}, error:"", isLoading: true });

  fetch(requestURL, {
    method: "GET"
  })
    .then(response => response?.json() )
    .then(data => {
      showRepo({ data, error: "", isLoading: false });
    })
    .catch (error => {
      showRepo({ data: {}, error: error.message, isLoading: false })
    }) 
    
}

const showContent = document.getElementById('showContent');
const buttonShow = document.querySelector("#show");
const inputSearch = document.querySelector("#search");
// const buttonSearch = document.querySelector("#searchRepo");
inputSearch.value = 'JavaScript';
inputSearch.disabled = true;
// buttonSearch.disabled = true;




const myArticle = document.createElement('article');
const myPara1 = document.createElement('p');
const paragraph = document.createElement('p');
const myList = document.createElement('ul');



buttonShow.onclick = () => getRepositories(inputSearch.value); 
// buttonSearch.onclick = () => filter();
// buttonSearch.addEventListener('click', getRepositories(inputSearch.value));

/*
function filter(dataFilter){
  console.log(inputSearch.value);
  document.getElementById('showContent').innerHTML = "";
  
  const texto = inputSearch.value.toLowerCase();

  for (let repoFilter of repo){
    let name = repoFilter.description.toLowerCase();
    if(name.indexOf(texto) != -1){
      console.log(repoFilter);
    }
  }
}
*/
function showRepo(dataRepositories){

  /* loading */
  if(dataRepositories.isLoading){ 
    showContent.innerHTML = '';
    paragraph.textContent = "Searching Repositories..."
    showContent.appendChild(paragraph);  
    return;
  } 
  
  /* error */ 
  if (dataRepositories.error){
    showContent.innerHTML = '';
    paragraph.textContent = dataRepositories.error;
    paragraph.className = 'error';
    showContent.appendChild(paragraph);
       
    return;
  }
  
  let repositories = dataRepositories.data.items;
  // console.log(repo);
  showContent.innerHTML = '';
  myList.innerHTML='';
  if (repositories.length > 0) {
    paragraph.textContent = 'Repositories for ' + inputSearch.value + ':';
  }
  repositories.forEach(repository => {
    let repoName = repository.id;
    if (repository.description != null) {
      repoName = repoName  + ' - ' + repository?.description;
    }
    var listItem = "";
    listItem = document.createElement('li');
    listItem.textContent = repoName;
    console.log(listItem);
    myList.appendChild(listItem);
  });
     
  myArticle.appendChild(paragraph);
  myArticle.appendChild(myList);

  showContent.appendChild(myArticle);
  //buttonSearch.disabled = false; 
  inputSearch.value = '';
  inputSearch.disabled = false;
      
}
