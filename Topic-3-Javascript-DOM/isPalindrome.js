function isPalindrome(word) {
  
  if (typeof word !== 'string') {
    alert('The type of the argument word is not a string');
    throw new Error('The type of the argument word is not a string');
  }

  if (word === ''){
    alert('The type of the argument word is empty');
    throw new Error('The type of the argument word is not a string');
  }

  const wordReversed = word
    .toLowerCase()
    .split("")
    .reverse()
    .join("");
  
  return word.toLowerCase() === wordReversed;  
}