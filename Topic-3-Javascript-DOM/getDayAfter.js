function getDayAfter(daySearch,addDay) {
 
  const daysOfWeek = ['Sunday','Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
  const dayFindedIndex = daysOfWeek.indexOf(daySearch);
  
  if (dayFindedIndex < 0) {
    alert('The daySearch argument is invalid');
    throw new Error('The daySearch is invalid');
  } 

  if (Number(addDay) < 0) {
    alert('The addDay argument is not positive');
    throw new Error('The addDay argument is not positive');
  } 

  if (typeof Number(addDay) !== 'number') {
    alert('The addDay argument is not type number');
    throw new Error('The addDay argument is not type number');
  }

  const reminder = (dayFindedIndex + Number(addDay)) % daysOfWeek.length;

  return daysOfWeek[reminder];

  }