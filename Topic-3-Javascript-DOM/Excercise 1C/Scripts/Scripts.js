function isPalindrome(word) {
  
  if (typeof word !== 'string') {
    alert('The type of the argument word is not a string');
    throw new Error('The type of the argument word is not a string');
  }

  if (word === ''){
    alert('The type of the argument word is empty');
    throw new Error('The type of the argument word is not a string');
  }

  const wordReversed = word
    .toLowerCase()
    .split("")
    .reverse()
    .join("");
  
  // return word.toLowerCase() === wordReversed;  

  if(word.toLowerCase() === wordReversed){
    alert(word.toLowerCase() + ". It's Palindrome")
  }
  else {
    alert(word.toLowerCase() + ". It's not Palindrome")
  }
}


function getDayAfter(daySearch,addDay) {
    
  const daysOfWeek = ['Sunday','Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
  const dayFindedIndex = daysOfWeek.indexOf(daySearch);
  
  if (dayFindedIndex < 0) {
    alert('The daySearch argument is invalid');
    throw new Error('The daySearch is invalid');
  } 

  if (Number(addDay) < 0) {
    alert('The addDay argument is not positive');
    throw new Error('The addDay argument is not positive');
  } 

  if (typeof Number(addDay) !== 'number') {
    alert('The addDay argument is not type number');
    throw new Error('The addDay argument is not type number');
  }

  const reminder = (dayFindedIndex + Number(addDay)) % daysOfWeek.length;

  return daysOfWeek[reminder];
  const newDay = daysOfWeek[reminder];
  
  alert('The new day is ' + newDay)
  
  }