class Actor {
  name;
  age;

  constructor(name, age) {
    this.name = name;
    this.age = age;
  }

}
class EventEmitter {
  constructor() {
    this.events = {};
  }

  on(eventName, callback) {
    this.events[eventName] = callback;
    console.log('Events: ' + eventName + ' was Added');
  }

  emit(eventName) {
    this.events[eventName]();
    log = new Logger();
    log.log(eventName);
  }

  off(eventName, callback) {
    delete this.events[eventName];
    console.log('Events: ' + eventName + ' was Deleted');
  }

}
class Logger {
  constructor() {}

  log(info) {
    let message = 'The ' + info + ' event has been emitted';
    console.log(message);
  }

}
class Movie extends EventEmitter {
  title;
  year;
  duration;

  constructor(title, year, duration, actors) {
    super();
    this.title = title;
    this.year = year;
    this.duration = duration;
    this.actors = [];
  }

  addCast(actors) {
    // console.log (actors)
    if (this.actors.length == 0) {
      this.actors = [...actors];
    } else {
      actors.forEach(actor => {
        let found = this.actors.findIndex(aux => aux.name === actor.name);
        console.log(found2);

        if (found === -1) {
          this.actors.push(actor);
        } else {
          this.actors[found] = actor;
        }
      });
    }

    ;
  }

}
