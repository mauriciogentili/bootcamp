class Movie {
  title;
  year;
  duration;

  constructor(title, year,duration){
    this.title = title;
    this.year = year;
    this.duration = duration;
  }

  play(){
    console.log('estamos dando play a la peli a: ' + this.title);
  }

  pause(){
    console.log('Peli en pausa: ' + this.title);
  }

  resume(){
    console.log('Peli en Resume: ' + this.title);
  }
}

// mixin
let social = {
  shared(friendName) {
    alert('Shared with ' + friendName);
  },
  
  like(friendName) {
    alert('Like to ' + friendName);
  }
};

Object.assign(Movie.prototype, social);


