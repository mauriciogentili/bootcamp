class Movie {
  title;
  year;
  duration;

  constructor(title, year,duration){
    this.title = title;
    this.year = year;
    this.duration = duration;
  }

  play(){
    console.log('estamos dando play a la peli a: ' + this.title);
  }

  pause(){
    console.log('Peli en pausa: ' + this.title);
  }

  resume(){
    console.log('Peli en Resume: ' + this.title);
  }
}

class Actor{
  name;
  age;

  constructor (name, age){
    this.name = name;
    this.age = age
  }
} 

class EventEmitter {

  constructor(){
    this.events = {}
  }

  on(eventName, callback){  
    this.events[eventName] = callback
    console.log('Events: ' + eventName + ' was Added')
  };

  emit(eventName){
    this.events[eventName]()
  };
  
  off(eventName, callback){ 
    delete this.events[eventName];
    console.log('Events: ' + eventName + ' was Deleted')
  };
}

function play(){
  console.log("It's Play")
}

function pause(){
  console.log("It's Pause")
}

function stop(){
  console.log("It's Stop")
}

evento = new EventEmitter
evento.on('play',play)
evento.on('stop',stop)

