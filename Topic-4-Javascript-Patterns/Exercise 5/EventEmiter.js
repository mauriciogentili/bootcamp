class EventEmitter {

  constructor(){
    this.events = {}
  }

  on(eventName, callback){  
    this.events[eventName] = callback
    console.log('Events: ' + eventName + ' was Added')
  };

  emit(eventName){
    this.events[eventName]()
    log = new Logger;
    log.log(eventName);
  };
  
  off(eventName, callback){ 
    delete this.events[eventName];
    console.log('Events: ' + eventName + ' was Deleted')
  };
}
