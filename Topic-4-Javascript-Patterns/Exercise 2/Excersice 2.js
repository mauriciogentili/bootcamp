class EventEmitter {

  constructor(){
    this.events = {}
  }

  on(eventName, callback){  
    this.events[eventName] = callback
    console.log('Events: ' + eventName + ' was Added')
  };

  emit(eventName){
    this.events[eventName]()
  };
  
  off(eventName, callback){ 
    delete this.events[eventName];
    console.log('Events: ' + eventName + ' was Deleted')
  };
}

class Movie extends EventEmitter {
  title;
  year;
  duration;

  constructor(title, year, duration, actors){
    super();
    this.title = title;
    this.year = year;
    this.duration = duration;
    this.actors = this.actors;
  }
}

function play(){
  console.log("It's Play")
}

function pause(){
  console.log("It's Pause")
}

function stop(){
  console.log("It's Stop")
}

function resume(){
  console.log("This movie is ........")
}

evento = new EventEmitter
evento.on('play',play)
evento.on('stop',stop)

