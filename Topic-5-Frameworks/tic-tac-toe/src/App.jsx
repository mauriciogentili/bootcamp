import React from 'react';
import './App.css';
// import Board from './components/Board';
import Game from './components/Game';
// import Square from './components/Square';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <Game />
      </header>
    </div>
  );
}

export default App;
