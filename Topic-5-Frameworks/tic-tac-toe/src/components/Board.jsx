import React, { useState } from 'react';
import Square from './Square';

function Board() {
  const title = 'Tic Tac Toe';
  const [state, setState] = useState({
    squares: Array(9).fill(null),
    xIsNext: true,
    lineWin: Array(9).fill(null),
  });

  let status = `Next player: ${state.xIsNext ? 'X' : 'O'}`;
  let winner;

  function calculateWinner(aSquares) {
    const lines = [
      [0, 1, 2],
      [3, 4, 5],
      [6, 7, 8],
      [0, 3, 6],
      [1, 4, 7],
      [2, 5, 8],
      [0, 4, 8],
      [2, 4, 6],
    ];
    // eslint-disable-next-line no-plusplus
    for (let i = 0; i < lines.length; i++) {
      const [a, b, c] = lines[i];
      if (aSquares?.[a] && aSquares[a] === aSquares[b] && aSquares[a] === aSquares[c]) {
        const wLine = Array(9).fill(null);
        wLine[a] = true;
        wLine[b] = true;
        wLine[c] = true;
        // setState({ lineWin: wLine });
        setState({ squares: aSquares, xIsNext: !state.xIsNext, lineWin: wLine });
        return aSquares[a];
      }
    }
    return null;
  }

  function handleClick(i) {
    const aSquares = state.squares.slice();
    const aLineWin = state.lineWin.slice();
    if (calculateWinner(aSquares) || state.squares[i]) {
      return;
    }
    status = `Next player: ${state.xIsNext ? 'X' : 'O'}`;
    aSquares[i] = state.xIsNext ? 'X' : 'O';
    setState({ squares: aSquares, xIsNext: !state.xIsNext, lineWin: aLineWin });
    if (calculateWinner(aSquares)) {
      winner = aSquares[i];
      status = 'WINNER';
      // eslint-disable-next-line no-useless-return
      return;
    }
  }

  function renderSquare(i) {
    return (
      <Square
        value={state.squares[i]}
        onClick={() => handleClick(i)}
        win={state.lineWin[i]}
      />
    );
  }

  return (
    <div>
      <div className="title">{title}</div>

      { winner ? <div className="winner">{status}</div> : <div>{status}</div> }

      <div className="board-row">
        {renderSquare(0)}
        {renderSquare(1)}
        {renderSquare(2)}
      </div>
      <div className="board-row">
        {renderSquare(3)}
        {renderSquare(4)}
        {renderSquare(5)}
      </div>
      <div className="board-row">
        {renderSquare(6)}
        {renderSquare(7)}
        {renderSquare(8)}
      </div>
    </div>
  );
}

export default Board;
