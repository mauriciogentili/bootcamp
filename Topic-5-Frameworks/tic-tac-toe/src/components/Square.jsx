import React from 'react';

function Square(props) {
  let lw = 'square';
  if (props.win) {
    lw += '-win';
  }
  return (

    // eslint-disable-next-line no-console
    <button type="button" className={lw} onClick={() => props.onClick()}>
      {props.value}
    </button>
  );
}

export default Square;
