import React from 'react';
import {
  AppBar, Toolbar, IconButton, Typography, Button, Avatar, Badge,
} from '@mui/material';
import { Menu as MenuIcon, Message as MailIcon } from '@mui/icons-material';

function Topbar(props) {
  return (
    <AppBar position="fixed" color="secondary">
      <Toolbar>
        <IconButton edge="start" color="inherit" onClick={props.toggleLeftBar}>
          <MenuIcon />
        </IconButton>
        <Typography variant="h6" color="inherit" style={{ flexGrow: 1 }} noWrap> APP </Typography>
        <Badge badgeContent={4} color="success">
          <MailIcon color="action" />
        </Badge>
        <Button variant="text" color="inherit">Log out </Button>
        <Avatar sx={{ bgcolor: 'orange' }}>FD</Avatar>
      </Toolbar>
    </AppBar>

  );
}

export default Topbar;
