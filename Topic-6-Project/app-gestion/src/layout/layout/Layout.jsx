import React, { useState } from 'react';
// import { makeStyles } from '@mui/styles';
// import { Container } from '@mui/material';
import Topbar from '../topbar/Topbar';
import Leftbar from '../leftbar/Leftbar';
// import Footer from '../footer/Footer';
/*
const styles = makeStyles({
  styleCh2: {
    marginTop: '75px',
    marginLeft: '15px',
    marginRight: '15px',
  },
});
*/
function Layout({ children }) {
  // const classes = styles();
  const [open, setOpen] = useState(false);

  const styleChildren = {
    marginTop: '75px',
    marginLeft: '15px',
    marginRight: '15px',
  };

  const handleClick = () => {
    setOpen(!open);
  };

  return (
    <>
      <Topbar open={open} toggleLeftBar={handleClick} />
      <div style={styleChildren}>
        {children}
      </div>
      <Leftbar open={open} toggleLeftBar={handleClick} />
    </>
  );
}

export default Layout;

/*
      <div style={styleChildren}>

      </div>
      */
