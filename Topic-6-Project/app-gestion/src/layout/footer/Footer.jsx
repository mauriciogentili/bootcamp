import React from 'react';
import {
  Typography, Container,
} from '@mui/material';

function Footer() {
  return (
    <footer style={{ bottom: 0, position: 'fixed', width: '100%' }}>
      <Container maxWidth="sm">
        <Typography align="center">
          Zingaro Copyrigth
          {' '}
          {new Date().getFullYear()}
        </Typography>
      </Container>
    </footer>
  );
}

export default Footer;
