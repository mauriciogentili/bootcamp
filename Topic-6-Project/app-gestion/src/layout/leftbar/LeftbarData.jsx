import React from 'react';
import HomeIcon from '@mui/icons-material/Home';
import ContactsIcon from '@mui/icons-material/Contacts';

const LeftbarData = [
  {
    id: 1,
    title: 'Home',
    path: '/',
    icon: <HomeIcon />,
  },
  {
    id: 2,
    title: 'Customers',
    path: '/customers',
    icon: <ContactsIcon />,
  },
  {
    id: 3,
    title: 'Providers',
    path: '/providers',
    icon: <ContactsIcon />,
  },
  {
    id: 4,
    title: 'Products',
    path: '/products',
    icon: <ContactsIcon />,
  },
  {
    id: 5,
    title: 'Contact',
    path: '/contact',
    icon: <ContactsIcon />,
  },
];

export default LeftbarData;
