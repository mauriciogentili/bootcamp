import React from 'react';
import {
  IconButton, Drawer, Paper, Divider, List, ListItemIcon, ListItemText, ListItemButton,
} from '@mui/material';
// import Header from './Header';
import { Menu as MenuIcon } from '@mui/icons-material';
import { NavLink } from 'react-router-dom';
import LeftbarData from './LeftbarData';

function Leftbar(props) {
  return (
    <nav>
      <Drawer anchor="left" open={props.open} onClose={props.toggleLeftBar}>
        <Paper style={{ width: 240 }} elevation={0}>
          <div style={{ padding: 20 }}>
            <IconButton edge="start" color="inherit" onClick={props.toggleLeftBar}>
              <MenuIcon />
            </IconButton>
          </div>
          <Divider />
          <List>
            {
              LeftbarData.map((item) => (
                <NavLink to={item.path} key={item.id}>
                  <ListItemButton
                    key={item.id}
                    onClick={props.toggleLeftBar}
                  >
                    <ListItemIcon>
                      {item.icon}
                    </ListItemIcon>
                    <ListItemText>
                      {item.title}
                    </ListItemText>
                  </ListItemButton>
                </NavLink>
              ))
            }
          </List>
        </Paper>
      </Drawer>
    </nav>
  );
}

export default Leftbar;
