import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import { ThemeProvider } from '@mui/material/styles';
import CssBaseline from '@mui/material/CssBaseline';
import theme from './ThemeConfig';
import Layout from './layout/layout/Layout';
import RoutersConfig from './routes/Routes';

import './App.css';

function App() {
  return (
    <ThemeProvider theme={theme}>
      <CssBaseline />
      <BrowserRouter>
        <Layout>
          <RoutersConfig />
        </Layout>
      </BrowserRouter>
    </ThemeProvider>
  );
}

export default App;
