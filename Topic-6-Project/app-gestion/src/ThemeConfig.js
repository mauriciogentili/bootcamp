import { createTheme } from '@mui/material/styles';

const theme = createTheme({
});

/*
const theme = createTheme({
  palette: {
    secondary: {
      main: '#757ce8',
    },
    background: {
      paper: '#f06292',
      default: '#757ce8',
    },
  },
});
*/
export default theme;
