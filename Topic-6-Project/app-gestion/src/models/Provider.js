class Provider {
  idProvider;
  name;
  address;
  phone;
  celular;
  email;
  city;

  constructor(idProvider, name, address, phone, mobile, email, city) {
    this.idProvider = idProvider;
    this.name = name;
    this.address = address;
    this.phone = phone;
    this.mobile = mobile;
    this.email = email;
    this.city = city;
  }
}

export default Provider;
