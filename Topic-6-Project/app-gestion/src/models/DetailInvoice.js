class DetailInvoice {
  line;
  idInvoice;
  quantity;
  idProduct;
  descriptionProduct;
  price;
  discont;
  total;

  constructor(idInvoice, line, quantity, idProduct, descriptionProduct, price, discont, total) {
    this.idInvoice = idInvoice;
    this.line = line;
    this.quantity = quantity;
    this.idProduct = idProduct;
    this.descriptionProduct = descriptionProduct;
    this.total = total;
    this.price = price;
    this.discont = discont;
  }
}

export default DetailInvoice;
