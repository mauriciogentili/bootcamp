class Customer {
  idCustomer;
  name;
  address;
  phone;
  celular;
  email;
  city;

  constructor(idCustomer, name, address, phone, mobile, email, city) {
    this.idCustomer = idCustomer;
    this.name = name;
    this.address = address;
    this.phone = phone;
    this.mobile = mobile;
    this.email = email;
    this.city = city;
  }
}

export default Customer;
