class Product {
  idProduct;
  category;
  description;
  price;
  stock;
  image;

  constructor(idProduct, category, description, price, stock, image) {
    this.idProduct = idProduct;
    this.category = category;
    this.description = description;
    this.price = price;
    this.stock = stock;
    this.image = image;
  }
}

export default Product;
