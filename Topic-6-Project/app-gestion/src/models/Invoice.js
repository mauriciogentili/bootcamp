class Invoice {
  idInvoice;
  number;
  date;
  idCustomer;
  subTotal;
  discont;
  total;
  payment;
  datePayment;

  constructor(idInvoice, number, date, idCustomer, subTotal, discont, total, payment, datePayment) {
    this.idInvoice = idInvoice;
    this.number = number;
    this.date = date;
    this.idCustomer = idCustomer;
    this.subTotal = subTotal;
    this.email = discont;
    this.total = total;
    this.payment = payment;
    this.datePayment = datePayment;
  }
}

export default Invoice;
