import React from 'react';
import { TextField } from '@mui/material';
import { makeStyles } from '@mui/styles';

const styles = makeStyles({
  searchText: {
    margin: '5px',
    align: 'rigth',
  },
});

function Search(props) {
  const classes = styles();
  return (
    <TextField
      className={classes.searchText}
      label="Search "
      variant="outlined"
      size="small"
      value={props.value}
      onChange={props.onChange}
      fullWidth
    />
  );
}

export default Search;
