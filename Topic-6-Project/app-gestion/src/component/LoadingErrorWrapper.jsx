import React from 'react';
import ErrorMessage from './ErrorMessage';
import Loading from './Loading';

function LoadingErrorWrapper(props) {
  const { isLoading, error, children } = props;
  // eslint-disable-next-line no-console
  // console.log(!isLoading && !error);
  return (
    <>
      {isLoading && <Loading isLoading={isLoading} />}
      {!!error && <ErrorMessage message={error} />}
      {(!isLoading && !error) && children}
    </>
  );
}

export default LoadingErrorWrapper;
