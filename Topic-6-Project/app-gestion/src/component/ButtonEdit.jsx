import React from 'react';
import { Button } from '@mui/material';
import { makeStyles } from '@mui/styles';

const styles = makeStyles({
  btn: {
    margin: '5px',
  },
});

function ButtonEdit(props) {
  const classes = styles();
  return (
    <Button
      className={classes.btn}
      color="warning"
      variant="contained"
      id="Edit"
      onClick={props.onClick}
      disabled={props.disabled}
    >
      {props.label}
    </Button>
  );
}

export default ButtonEdit;
