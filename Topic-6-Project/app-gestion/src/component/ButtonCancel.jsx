import React from 'react';
import { Button } from '@mui/material';
import { makeStyles } from '@mui/styles';

const styles = makeStyles({
  btn: {
    margin: '5px',
  },
});

function ButtonCancel(props) {
  const classes = styles();
  return (
    <Button
      className={classes.btn}
      variant="contained"
      id="Delete"
      onClick={props.onClick}
    >
      {props.label}
    </Button>
  );
}

export default ButtonCancel;
