import React from 'react';
import { Button } from '@mui/material';
import { makeStyles } from '@mui/styles';

const styles = makeStyles({
  btn: {
    margin: '5px',
  },
});

function ButtonCreate(props) {
  const classes = styles();
  return (
    <Button
      className={classes.btn}
      color="success"
      variant="contained"
      id="Create"
      onClick={props.onClick}
      disabled={props.disabled}
    >
      {props.label}
    </Button>
  );
}

export default ButtonCreate;
