import React from 'react';
import Stack from '@mui/material/Stack';
import Alert from '@mui/material/Alert';

function ErrorMessage(props) {
  const { message } = props;
  return (
    <Stack spacing={2} sx={{ width: '100%' }}>
      <Alert severity="error">{message}</Alert>
    </Stack>
  );
}

export default ErrorMessage;
