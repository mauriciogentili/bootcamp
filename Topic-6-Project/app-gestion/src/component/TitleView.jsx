import React from 'react';

import { Typography } from '@mui/material';

function TitleView(props) {
  return (
    <Typography
      color="inherit"
      variant="h4"
      component="div"
    >
      {props.title}
    </Typography>
  );
}

export default TitleView;
