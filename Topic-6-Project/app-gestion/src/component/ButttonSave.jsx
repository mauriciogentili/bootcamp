import React from 'react';
import { Button } from '@mui/material';
import { makeStyles } from '@mui/styles';

const styles = makeStyles({
  btn: {
    margin: '5px',
  },
});

function ButttonSave(props) {
  const classes = styles();
  return (
    <div>
      <Button
        className={classes.btn}
        variant="contained"
        id="Delete"
        onClick={props.onClick}
        type={props.type}
      >
        {props.label}
      </Button>
    </div>
  );
}

export default ButttonSave;
