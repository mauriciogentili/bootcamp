import React from 'react';
import { Routes, Route } from 'react-router-dom';
import Home from '../views/home/Home';
import Contact from '../views/contact/Contact';
import NotFound from '../views/notFound/NotFound';
import Customers from '../views/customers/Customers';
import ProductsView from '../views/products/Products';
import Providers from '../views/providers/Providers';
import AddNewProduct from '../views/products/AddNewProduct';
import UpdateProduct from '../views/products/UpdateProduct';
import DetailProduct from '../views/products/DetailProduct';

function RoutersConfig() {
  return (
    <Routes>
      <Route path="/" element={<Home />} />
      <Route path="/home" element={<Home />} />
      <Route path="/customers" element={<Customers />} />
      <Route path="/products" element={<ProductsView />} />
      <Route path="/products/new" element={<AddNewProduct />} />
      <Route path="/products/update/:id" element={<UpdateProduct />} />
      <Route path="/products/detail/:id" element={<DetailProduct />} />
      <Route path="/providers" element={<Providers />} />
      <Route path="/contact/*" element={<Contact />} />
      <Route path="*" element={<NotFound />} />
    </Routes>
  );
}

export default RoutersConfig;
