/* eslint-disable no-console */
/* eslint-disable no-useless-catch */
import { db } from './config';
import { FIREBASE_ADMIN_LOGGED } from './shared';

const NON_AUTHORIZED = 'Non Authorized';
const EMPTY_COLLECTION_ERROR_MESSAGE = "Collection don't exist or is empty";
const UNEXISTING_DOCUMENT_ERROR_MESSAGE = "Document don't exist";

/*
  * To get all the documents of a firebase collection, similar to a GET search query
  * @param {string} collectionName - The name of the firebase collection
*/

const getDocumentsListFromCollection = async (collectionName) => {
  const collection = db.collection(collectionName);
  const documents = [];
  const querySnapshot = await collection.get();
  if (querySnapshot.empty) {
    throw Error(EMPTY_COLLECTION_ERROR_MESSAGE);
  }
  querySnapshot.forEach((doc) => {
    documents.push({ id: doc.id, ...doc.data() });
  });

  return documents;
};

/*
  * To get a specific document of a firebase collection, similar to a GET method
  * @param {string} collectionName - The name of the firebase collection
  * @param {string} documentId - The id of the firebase document
*/

const getDocumentFromCollection = async (collectionName, documentId) => {
  try {
    const document = await db.collection(collectionName).doc(documentId).get();
    if (!document.exists) {
      throw new Error(UNEXISTING_DOCUMENT_ERROR_MESSAGE);
    }

    return document.data();
  } catch (error) {
    throw error;
  }
};

/*
  * To get a specific document of a firebase collection, similar to a POST/PUT method
  * @param {string} collectionName - The name of the firebase collection
  * @param {object} newDocument - The new document that will be added to the collection
*/

const addDocumentFromCollection = async (collectionName, newDocument) => {
  try {
    if (!FIREBASE_ADMIN_LOGGED) {
      throw new Error(NON_AUTHORIZED);
    }
    const documents = db.collection(collectionName).doc();
    await documents.set(newDocument);
  } catch (error) {
    throw error;
  }
};

/*
  * To get a specific document of a firebase collection, similar to a POST/PUT method
  * @param {string} collectionName - The name of the firebase collection
  * @param {object} newDocument - The new document that will be replaced to the existing one
    on the collection.
  * @param {string} documentId - The id of the firebase document that will be updated.
*/

const updateDocumentFromCollection = async (
  collectionName,
  newDocument,
  documentId,
) => {
  try {
    if (!FIREBASE_ADMIN_LOGGED) {
      throw new Error(NON_AUTHORIZED);
    }
    await db.collection(collectionName).doc(documentId).update(newDocument);
  } catch (error) {
    throw error;
  }
};

/*
  * To get a specific document of a firebase collection, similar to a DELETE method
  * @param {string} collectionName - The name of the firebase collection
  * @param {string} documentId - The id of the firebase document that will be deleted.
*/

const deleteDocumentFromCollection = async (collectionName, documentId) => {
  if (!FIREBASE_ADMIN_LOGGED) {
    throw new Error(NON_AUTHORIZED);
  }
  try {
    const document = db.collection(collectionName).doc(documentId);
    const documentData = await document.get();
    if (!documentData.exists) {
      throw new Error(UNEXISTING_DOCUMENT_ERROR_MESSAGE);
    }
    await document.delete();
  } catch (error) {
    throw error;
  }
};

export {
  getDocumentsListFromCollection,
  getDocumentFromCollection,
  addDocumentFromCollection,
  updateDocumentFromCollection,
  deleteDocumentFromCollection,
};
