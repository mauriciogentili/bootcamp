/* eslint-disable max-len */
import { useState, useEffect } from 'react';

const useFirebaseDataToState = (getMethod) => {
  const [firebaseData, setFirebaseData] = useState({
    data: undefined,
    loading: false,
    error: '',
  });
  const title = 'aa';
  const [forceRefetch, setForceRefetch] = useState(0);

  const dependencies = [forceRefetch];

  const reload = () => setForceRefetch(forceRefetch + 1);

  const setError = (error) => setFirebaseData(({ data: undefined, loading: false, error }));
  const setData = (data) => setFirebaseData(({ data, loading: false, error: '' }));
  const setLoading = (loading) => setFirebaseData(({ data: undefined, loading, error: '' }));

  useEffect(async () => {
    setLoading(true);
    try {
      const response = await getMethod();
      setData(response);
    } catch (error) {
      setError(error.message);
    }
  }, dependencies);

  return [firebaseData, reload, title];
};

export default useFirebaseDataToState;
