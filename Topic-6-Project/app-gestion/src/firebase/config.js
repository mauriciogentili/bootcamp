import firebase from 'firebase';

// A configuration object will be sent to you by the course tutor.
const firebaseConfig = {
  apiKey: 'AIzaSyA1UAsdNXjVxNJLnnZ1DMzYTT2ynsI0RJI',
  authDomain: 'giga-bootcamp.firebaseapp.com',
  projectId: 'giga-bootcamp',
  storageBucket: 'giga-bootcamp.appspot.com',
  messagingSenderId: '265157662743',
  appId: '1:265157662743:web:7128ff3b5ceda027552452',
  measurementId: 'G-PG150NTMTE',
};

const firebaseApp = firebase.initializeApp(firebaseConfig);
const db = firebaseApp.firestore();

export { db };
