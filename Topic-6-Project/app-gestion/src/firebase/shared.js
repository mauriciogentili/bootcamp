import firebase from 'firebase';

const firebaseCurrentUser = firebase.auth()?.currentUser;
const FIREBASE_ADMIN_LOGGED = true;
// !!firebaseCurrentUser?.uid;

export { firebaseCurrentUser, FIREBASE_ADMIN_LOGGED };
