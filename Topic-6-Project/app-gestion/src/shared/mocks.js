import Product from '../models/Product';
import Category from '../models/Category';
import Customer from '../models/Customer';
import Provider from '../models/Provider';

export const ListProducts = [
  new Product(1, 'Chocolates', 'Milka', 100000, 1, ''),
  new Product(2, 'Caramelos', 'Palitos de la Selva', 50000, 3, ''),
  new Product(3, 'Chicles', 'Bazooka', 500000, 1, ''),
  new Product(4, 'Galletitas', 'Pepitos', 250000, 1, ''),
];

export const ListCategories = [
  new Category(1, 'Chocolates'),
  new Category(2, 'Chicles'),
  new Category(3, 'Galletitas'),
  new Category(4, 'Caramelos'),
];

export const ListCustomers = [
  new Customer(1, 'David Tissera', 'Salta 2020', '', '3416152362', 'dtissera@gg.com', 'Rosario'),
  new Customer(2, 'Mauricio Gentili', 'Zeballos 2684', '', '3416100665', 'mgentili@gg.com', 'Casilda'),
  new Customer(3, 'Maxiiliano Tulian', 'Corrientes 350', '', '341660302', 'mtulian@gg.com', 'Rosario'),
  new Customer(4, 'Geronimo Palacios', 'San Juan 1900', '', '3415987588', 'gpalacios@gg.com', 'Sunchalez'),
];

export const ListProviders = [
  new Provider(1, 'La Brujula', 'Corrientes 1460', '', '3416152362', 'labrujula@gg.com', 'Rosario'),
  new Provider(2, 'la Sede', 'San Lorenzo 1399', '', '3416100665', 'lasde@gg.com', 'Rosario'),
  new Provider(3, 'Giga IT', 'Rivadavia 2355', '', '341660302', 'gigait@gg.com', 'Casilda'),
];
