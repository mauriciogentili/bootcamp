import React from 'react';
import { ListCustomers } from '../../shared/Mocks';

function Customers() {
  return (
    <div>
      <h2>Customers</h2>
      <p />
      <div>
        <table>
          <tr>
            <td>Id</td>
            <td>Name</td>
            <td>E-mail</td>
          </tr>
          {
            ListCustomers.map((item) => (
              <tr>
                <td>{item.id}</td>
                <td>{item.name}</td>
                <td>{item.email}</td>
              </tr>
            ))
          }
        </table>
      </div>
    </div>
  );
}

export default Customers;
