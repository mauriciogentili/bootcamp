/* eslint-disable no-alert */
/* eslint-disable no-restricted-globals */
/* eslint-disable no-console */
/* eslint-disable no-unused-vars */
import React, { useState } from 'react';
import {
  Grid, TextField, Dialog, DialogTitle, DialogContent, DialogContentText,
  DialogActions, Button, Alert, Snackbar, Paper, Container, Typography,
} from '@mui/material';
import { useNavigate, useParams } from 'react-router-dom';
import { useForm, Controller } from 'react-hook-form';
import ButtonCancel from '../../component/ButtonCancel';
import ButttonSave from '../../component/ButttonSave';
import { addDocumentFromCollection, updateDocumentFromCollection } from '../../firebase/cruds';

function ProductForm(props) {
  const [openModalConfirm, setOpenModalConfirm] = useState(false);
  const { defaultValues } = props;
  const { control, handleSubmit } = useForm({
    defaultValues,
  });
  const titleForm = !props.isUpdate ? 'New Product' : 'Edit Product';
  const navigate = useNavigate();

  const handleModalConfirm = () => {
    setOpenModalConfirm(!openModalConfirm);
  };

  async function handleSaved(data) {
    if (!props.isUpdate) {
      try {
        await addDocumentFromCollection('mgentili-products', data);
        alert('Saved');
      } catch (error) {
        alert(error.message);
      }
    } else {
      try {
        await updateDocumentFromCollection('mgentili-products', data, props.id);
        alert(`Update ${props.id}`);
      } catch (error) {
        alert(error.message);
      }
    }
    navigate('/products');
  }

  function handleCancel() {
    alert('Canceled');
    navigate('/products');
  }

  function formSubmit(data) {
    const result = confirm('Would you like to save?');
    if (result) {
      handleSaved(data);
    }
  }

  function openDialogConfirm() {
    return (
      <>
        <DialogTitle id="alert-dialog-title">
          Use Google location service?
        </DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            Let Google help apps determine location. This means sending anonymous
            location data to Google, even when no apps are running.
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleModalConfirm}>Disagree</Button>
          <Button onClick={handleModalConfirm} autoFocus>
            Agree
          </Button>
        </DialogActions>
      </>
    );
  }

  return (
    <div>
      <form onSubmit={handleSubmit(formSubmit)}>
        <Container component="main" maxWidth="sm" sx={{ mb: 4 }}>
          <Paper
            variant="outlined"
            sx={{ my: { xs: 3, md: 6 }, p: { xs: 2, md: 3 } }}
          >
            <Grid item>
              <Typography component="h1" variant="h4" align="center">
                {titleForm}
              </Typography>
            </Grid>
            <Grid item container spacing={2}>
              <Grid item xs={12}>
                <Controller
                  name="idProduct"
                  control={control}
                  render={({ field }) => (
                    <TextField
                      type="number"
                      label="Code"
                      variant="standard"
                      required
                      disabled={!!props.isUpdate}
                      {...field}
                    />
                  )}
                />
              </Grid>
              <Grid item xs={12}>
                <Controller
                  name="description"
                  control={control}
                  render={({ field }) => (
                    <TextField
                      label="Description"
                      variant="standard"
                      fullWidth
                      {...field}
                    />
                  )}
                />
              </Grid>
              <Grid item xs={12}>
                <Controller
                  name="category"
                  control={control}
                  render={({ field }) => (
                    <TextField
                      label="Category"
                      variant="standard"
                      fullWidth
                      {...field}
                    />
                  )}
                />
              </Grid>
              <Grid item xs={12}>
                <Controller
                  name="price"
                  control={control}
                  render={({ field }) => (
                    <TextField
                      label="Price"
                      variant="standard"
                      {...field}
                    />
                  )}
                />
              </Grid>
              <Grid item xs={12}>
                <Controller
                  name="stock"
                  control={control}
                  render={({ field }) => (
                    <TextField
                      label="Stock"
                      variant="standard"
                      {...field}
                    />
                  )}
                />
              </Grid>
            </Grid>
            <Grid item container spacing={2}>
              <Grid item xs={3}>
                <ButttonSave
                  type="submit"
                  label="Save"
                />
              </Grid>
              <Grid item xs={3}>
                <ButtonCancel
                  onClick={() => handleCancel()}
                  label="Cancel"
                />
              </Grid>
            </Grid>
          </Paper>
        </Container>
      </form>
      <div>
        <Dialog
          open={openModalConfirm}
          onClose={handleModalConfirm}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
        >
          {openDialogConfirm}
        </Dialog>
      </div>
    </div>
  );
}

export default ProductForm;
