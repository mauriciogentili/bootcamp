/* eslint-disable no-restricted-globals */
/* eslint-disable no-console */
/* eslint-disable no-alert */
import React, { useState, useEffect } from 'react';
import {
  Grid, Button, Modal, Box, Typography,
} from '@mui/material';
import { useNavigate } from 'react-router-dom';
import DataTable from '../../component/DataTable';
import Search from '../../component/Search';
import ButtonCreate from '../../component/ButtonCreate';
import ButtonEdit from '../../component/ButtonEdit';
import ButtonDelete from '../../component/ButtonDelete';
import TitleView from '../../component/TitleView';

import { getDocumentsListFromCollection, deleteDocumentFromCollection } from '../../firebase/cruds';
import useFirebaseDataToState from '../../firebase/hooks';
import LoadingErrorWrapper from '../../component/LoadingErrorWrapper';

const style = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: 400,
  bgcolor: 'background.paper',
  border: '2px solid #000',
  boxShadow: 24,
  p: 4,
};

function Products() {
  const getMethod = () => getDocumentsListFromCollection('mgentili-products');
  const [productsDataState, reload] = useFirebaseDataToState(getMethod);
  const { data = [], loading, error } = productsDataState;

  const [openModalDelete, setOpenModalDelete] = useState(false);

  const [valueSearched, setValueSearched] = useState('');

  const rowDeleted = [];
  const [rowsChecked, setRowsChecked] = useState(
    Array(data.length).fill(false),
  );

  const titleView = 'Products';
  const navigate = useNavigate();
  const column = [
    { id: 10, label: 'Id', field: 'idProduct', numeric: false, disablePadding: false },
    { id: 20, label: 'Description', field: 'description', numeric: false, disablePadding: false },
    { id: 30, label: 'Category', field: 'category', numeric: false, disablePadding: false },
    { id: 40, label: 'Price', field: 'price', numeric: true, disablePadding: false },
    { id: 50, label: 'Stock', field: 'stock', numeric: true, disablePadding: false },
  ];

  const handleAddProduct = () => {
    navigate('/products/new');
  };

  const handleEditProduct = () => {
    const rowIndex = rowsChecked.findIndex((rowChecked) => rowChecked === true);
    const { id } = data[rowIndex];
    navigate(`/products/update/${id}`);
  };

  const handleDetailProduct = (rowSelectedDetail) => {
    const { id } = data[rowSelectedDetail];
    navigate(`/products/detail/${id}`);
  };

  const handleDeleteProduct = (action) => {
    rowsChecked.forEach((rowChecked, index) => {
      if (rowChecked === true) {
        const { id } = data[index];
        rowDeleted.push(id);
      }
    });
    const result = confirm(`Would you like delete ${rowDeleted.length} products?`);
    if (result) {
      rowDeleted.forEach(async (itemDelete) => {
        try {
          await deleteDocumentFromCollection('mgentili-products', itemDelete);
          alert('Deleted products');
          reload();
        } catch (errorDelete) {
          alert(errorDelete.message);
        }
      });
    }

    if (action === 'Delete') {
      alert('Delete products');
    }
    setOpenModalDelete(openModalDelete);
  };

  const handleChangeCheckbox = (event, rowIndex) => {
    const newRowsChecked = [...rowsChecked];
    newRowsChecked[rowIndex] = !newRowsChecked[rowIndex];
    setRowsChecked(newRowsChecked);
  };

  const handleChangeAllCheckbox = () => {
    let newRowsChecked = [...rowsChecked];
    const allChecked = rowsChecked.findIndex((rowChecked) => rowChecked === false) === -1;

    if (allChecked) {
      newRowsChecked = newRowsChecked.fill(false);
    } else {
      newRowsChecked = newRowsChecked.fill(true);
    }
    setRowsChecked(newRowsChecked);
  };

  const isRowChecked = (rowIndex) => rowsChecked[rowIndex];
  const rowsSelected = rowsChecked.filter((rowChecked) => rowChecked);
  const isSomeRowChecked = rowsChecked.some((rowChecked) => rowChecked === true);
  const hasMoreThanOneRowChecked = rowsSelected.length !== 1;

  function search(rows) {
    const columns = rows[0] && Object.keys(rows[0]);
    return rows.filter((row) => columns.some(
      (columna) => row[columna]
        .toString()
        .toLowerCase()
        .indexOf(valueSearched.toLowerCase()) > -1,
    ));
  }
  useEffect(() => search(data), [data]);

  const bodyModal = (
    <Box sx={style}>
      <Typography id="modal-modal-title" variant="h6" component="h2">
        Delete
        {' '}
        {rowDeleted.length}
        {' '}
        Product?
      </Typography>
      <Button onClick={() => handleDeleteProduct('Delete')}>Delete</Button>
      <Button onClick={() => handleDeleteProduct()}>Cancel</Button>
    </Box>
  );

  return (
    <LoadingErrorWrapper isLoading={loading} error={error}>
      <Grid container spacing={2}>
        <Grid item container>
          <Grid item xs={12}>
            <TitleView title={titleView} />
          </Grid>
        </Grid>
        <Grid item container spacing={3}>
          <Grid item xs={9}>
            <ButtonCreate
              onClick={handleAddProduct}
              label="New Product"
            />
            <ButtonEdit
              onClick={handleEditProduct}
              disabled={hasMoreThanOneRowChecked}
              label="Edit"
            />
            <ButtonDelete
              onClick={handleDeleteProduct}
              disabled={!isSomeRowChecked}
              label="Delete"
            />
          </Grid>
          <Grid item xs={3}>
            <Search
              value={valueSearched}
              onChange={(e) => setValueSearched(e.target.value)}
            />
          </Grid>
        </Grid>
        <Grid item xs={12}>
          <DataTable
            headers={column}
            rows={search(data)}
            details={(rowSelectedDetail) => handleDetailProduct(rowSelectedDetail)}
            onChangeCheckbox={handleChangeCheckbox}
            onChangeAllCheckbox={handleChangeAllCheckbox}
            isRowChecked={isRowChecked}
            selected={rowsSelected.length}
          />
        </Grid>
        <div>
          <Modal
            open={openModalDelete}
            onClose={handleDeleteProduct}
          >
            {bodyModal}
          </Modal>
        </div>
      </Grid>
    </LoadingErrorWrapper>
  );
}

export default Products;
