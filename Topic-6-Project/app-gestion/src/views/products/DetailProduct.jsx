/* eslint-disable no-console */
/* eslint-disable no-alert */
import React from 'react';
import {
  Grid, Typography, Paper, Avatar, Container, Box,
} from '@mui/material';
import { useNavigate, useParams } from 'react-router-dom';
import { getDocumentFromCollection } from '../../firebase/cruds';
import useFirebaseDataToState from '../../firebase/hooks';
import LoadingErrorWrapper from '../../component/LoadingErrorWrapper';
import ButtonCancel from '../../component/ButtonCancel';

function DetailProduct() {
  const { id } = useParams();
  const getMethod = () => getDocumentFromCollection('mgentili-products', id);
  const [productsDataState] = useFirebaseDataToState(getMethod);
  const { data = [], loading, error } = productsDataState;
  const navigate = useNavigate();

  function handleBack() {
    navigate('/products');
  }

  return (
    <LoadingErrorWrapper isLoading={loading} error={error}>
      <Container component="main" maxWidth="sm" sx={{ mb: 4 }}>
        <Paper
          variant="outlined"
          sx={{ my: { xs: 3, md: 6 }, p: { xs: 2, md: 3 } }}
        >
          <Grid container spacing={2}>
            <Grid item xs={12}>
              <Typography variant="h6" gutterBottom>
                {data.description}
              </Typography>
            </Grid>
            <Grid item xs={6}>
              <Typography gutterBottom>
                Code:
              </Typography>
            </Grid>
            <Grid item xs={6}>
              <Typography gutterBottom>
                {data.idProduct}
              </Typography>
            </Grid>
            <Grid item xs={6}>
              <Typography gutterBottom>
                Category:
              </Typography>
            </Grid>
            <Grid item xs={6}>
              <Typography gutterBottom>
                {data.category}
              </Typography>
            </Grid>
            <Grid item xs={6}>
              <Typography gutterBottom>
                Price:
              </Typography>
            </Grid>
            <Grid item xs={6}>
              <Typography gutterBottom>
                {data.price}
              </Typography>
            </Grid>
            <Grid item xs={6}>
              <Typography gutterBottom>
                Stock:
              </Typography>
            </Grid>
            <Grid item xs={6}>
              <Typography gutterBottom>
                {data.stock}
              </Typography>
            </Grid>
            <Grid item xs={6}>
              <Typography gutterBottom>
                Image:
              </Typography>
            </Grid>
            <Grid item xs={6}>
              <Avatar alt="Photo" src={data.image} sx={{ width: 56, height: 56 }} />
            </Grid>
          </Grid>
          <Grid item container spacing={2}>
            <Grid item>
              <Box sx={{ display: 'flex', justifyContent: 'flex-end' }}>
                <ButtonCancel
                  onClick={() => handleBack()}
                  label="Back"
                />
              </Box>
            </Grid>
          </Grid>
        </Paper>
      </Container>
    </LoadingErrorWrapper>
  );
}

export default DetailProduct;
