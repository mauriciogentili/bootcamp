/* eslint-disable no-console */
import React from 'react';
import { useParams } from 'react-router-dom';
import ProductForm from './ProductForm';
import { getDocumentFromCollection } from '../../firebase/cruds';
import useFirebaseDataToState from '../../firebase/hooks';
import LoadingErrorWrapper from '../../component/LoadingErrorWrapper';

function UpdateProduct() {
  const { id } = useParams();
  const getMethod = () => getDocumentFromCollection('mgentili-products', id);
  const [productsDataState] = useFirebaseDataToState(getMethod);
  const { data = {}, loading, error } = productsDataState;

  const defaultValues = data;

  return (
    <LoadingErrorWrapper isLoading={loading} error={error}>
      <ProductForm isUpdate={!!id} productFiltered={data} defaultValues={defaultValues} id={id} />
    </LoadingErrorWrapper>
  );
}

export default UpdateProduct;
