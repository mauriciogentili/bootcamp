import React from 'react';
import {
  Container,
} from '@mui/material';
import Products from './Products';

function ProductsView() {
  return (
    <Container>
      <Products />
    </Container>
  );
}

export default ProductsView;
