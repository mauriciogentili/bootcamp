import React from 'react';
import ProductForm from './ProductForm';

function AddNewProduct() {
  const defaultValues = {
    idProduct: '',
    description: '',
    category: '',
    price: '',
    stock: '',
  };

  return (
    <ProductForm defaultValues={defaultValues} />
  );
}

export default AddNewProduct;
