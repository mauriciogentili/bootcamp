import React from 'react';
import { ListProviders } from '../../shared/Mocks';

function Providers() {
  return (
    <div>
      <h1>Providers</h1>
      <p />
      <div>
        <table>
          <tr>
            <td>Id</td>
            <td>Name</td>
            <td>E-mail</td>
          </tr>
          {
            ListProviders.map((item) => (
              <tr>
                <td>{item.id}</td>
                <td>{item.name}</td>
                <td>{item.email}</td>
              </tr>
            ))
          }
        </table>
      </div>
    </div>
  );
}

export default Providers;
